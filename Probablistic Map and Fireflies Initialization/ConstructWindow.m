function window = ConstructWindow(map, start, goal, heightOfWindow, widthOfWindow)

slopeAngle = atan2(goal(2) - start(2), goal(1) - start(1));

%% find X & Y coordinates of the window corners 

% xRD, yRD: x & y of the Right-Down corner respectively
% xLU, yLU: x & y of the Left-Upper corner respectively
% and so on...
xRD = start(1) + heightOfWindow * cos(-pi/2 + slopeAngle);
yRD = start(2) + heightOfWindow * sin(-pi/2 + slopeAngle); % DON'T FORGET TO CHECK THIS RULE
xLD = start(1) + heightOfWindow * cos(pi/2 + slopeAngle);
yLD = start(2) + heightOfWindow * sin(pi/2 + slopeAngle);% DON'T FORGET TO CHECK THIS RULE
xRU = xRD + 2*widthOfWindow*cos(slopeAngle);
yRU = yRD + 2*widthOfWindow*sin(slopeAngle);
xLU = xLD + 2*widthOfWindow*cos(slopeAngle);
yLU = yLD + 2*widthOfWindow*sin(slopeAngle);

% Construction of the window
xCorners = [xRD, xRU, xLU, xLD,  xRD];
yCorners = [yRD, yRU, yLU, yLD, yRD];
[numOfRows, numOfColumns] = size(map);
window = poly2mask(yCorners, xCorners, numOfRows, numOfColumns);

end