clc
close all;
clear all;
%% Initializaion
goal = zeros(1,2);
alpha = 0.5;
probabilityThresholdOfROI = 0.8;
numOfFireflies = 10;
coloredMap = imread('PathPlanningMap.png');
figure(1);
imshow(coloredMap);
grayMap = rgb2gray(coloredMap);
map = imresize(grayMap, 1);
invMap = imcomplement(map);
[numOfRows, numOfColumns] = size(invMap); %size of the map
figure(2);
imshow(grayMap);
hold on;
disp('Choose your start Point');
[yStartC, xStartC, but] = ginput(1);
h1 = text(yStartC,xStartC,'START', ...
                'HorizontalAlignment','center', ...
                'Color', [1 0 0], ...
                'FontSize',14);
xStart = round(xStartC);
yStart = round(yStartC);
start = [xStartC, yStartC];

% Validation test of Start Point
if invMap(xStart,yStart) > 0
    disp('You chose not Valid Start Point, Try again!');
    return;
end
disp('Choose your Goal Point');
[yGoalC, xGoalC, but2] = ginput(1);      % get a point
h2 = text(yGoalC,xGoalC,'GOAL', ...
                'HorizontalAlignment','center', ...
                'Color', [1 0 0], ...
                'FontSize',14);
xGoal = round(xGoalC);
yGoal = round(yGoalC);
goal = [xGoalC, yGoalC];

% Validation test of Goal Point
if invMap(xGoal,yGoal) > 0
    disp('You chose unavailable Start Point, Try again!');
    return;
end

%% Find Probabilities
maxOccupation = max(invMap(:));
[distance,slope] = DistanceBetweenEachPointAndLineSegement([numOfRows, numOfColumns],start, goal);
maxDist = max(distance(:));
probabilitiesMap = alpha * (repmat(double(maxDist), numOfRows, numOfColumns) -...
    distance) ./ repmat(double(maxDist), numOfRows, numOfColumns) + (1-alpha)* (repmat(double(maxOccupation), numOfRows, numOfColumns)-...
    double(invMap)) ./ repmat(double(maxOccupation), numOfRows, numOfColumns);

hold off;
figure(3);
imshow(probabilitiesMap);

%% Initilization of fireflies
fireflies = GenerateInitialFirefliesPopulation(numOfFireflies, invMap, probabilitiesMap, slope(1,1), start, goal, probabilityThresholdOfROI);
