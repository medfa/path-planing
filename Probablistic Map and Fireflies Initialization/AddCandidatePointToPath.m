function [newStartPoint, newStart, isGoalArrived, count] = AddCandidatePointToPath(map, probabilisticMap, slope, startPoint, goalPoint, probabilityThresholdOfROI, count)

heightOfWindow = 10;
widthOfWindow = heightOfWindow;
[numOfRows, numOfColumns] = size(map);
%probabilityDensityFunction = zeros(1,numOfRows* numOfColumns);
probabilityDensityIdx =  [];
window = ConstructWindow(map, startPoint, goalPoint, heightOfWindow, widthOfWindow);% Construct the first window
%figure(4);
%imshow(window);
regionOfInterest =  probabilisticMap .* window; % apply the window on the probabilistic map
%imshow(regionOfInterest);
isGoalArrived = regionOfInterest(goalPoint(1),goalPoint(2));
effeciencyOfROI = sum(sum(regionOfInterest));
numOfWindowCells = sum(sum(window));
expandingHeightCounter = 1;

while effeciencyOfROI < (probabilityThresholdOfROI * numOfWindowCells)
    
    heightOfWindow = heightOfWindow + heightOfWindow/2;% expand the height of the window
    window = ConstructWindow(map, startPoint, goalPoint, heightOfWindow, widthOfWindow);% Construct the window
    regionOfInterest =  probabilisticMap .* window; % apply the window(mask)on the probabilistic map
    effeciencyOfROI = sum(sum(regionOfInterest));
    numOfWindowCells = sum(sum(window));
    expandingHeightCounter = expandingHeightCounter +1;
    if expandingHeightCounter >= 3
        probabilityThresholdOfROI =probabilityThresholdOfROI * 3/4;
        expandingHeightCounter = 1;
    end
end

% Construct the Probability Density (PD)

if isGoalArrived>0
    newStartPointIdx = sub2ind([numOfRows, numOfColumns], goalPoint);
    newStartPoint = regionOfInterest(goalPoint(1),goalPoint(2));
    newStart = startPoint;
    
else
    normalizedProbability = regionOfInterest ./ effeciencyOfROI;
    minNormalizedProbability = min(normalizedProbability(normalizedProbability>0));
    [sortedProbs, probsIdx] = sort(normalizedProbability(:),'descend');
    probabilityDensityRepetition = sortedProbs ./ minNormalizedProbability;
    
    zeroIdx = find(sortedProbs==0,1);
    for i=1:zeroIdx-1
        %probabilityDensityFunction = cat(2,probabilityDensityFunction,repmat(sortedProbs(i),1, probabilityDensityRepetition(i)));
        probabilityDensityIdx = cat(1, probabilityDensityIdx, (repmat(probsIdx(i), 1, round(probabilityDensityRepetition(i))))');
    end
    Idx = find(randi(length(probabilityDensityIdx))<cumsum(1:length(probabilityDensityIdx)),1);
    newStartPointIdx = probabilityDensityIdx(Idx);
    [newStart(1),newStart(2)] = ind2sub([numOfRows, numOfColumns], newStartPointIdx);
    newStartPoint = map(newStart);
    
end
end