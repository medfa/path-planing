function fireflies = GenerateInitialFirefliesPopulation(numOfFireflies, map, probabilisticMap, slope, start, goal, occupancyThresholdOfROI)
counterOfFirefliesNumber = 1;
[numOfRows, numOfColumns] = size(map);
newStart = round(start);
fireflies{numOfFireflies} = [];
isGoalArrived =0;
rngSeed = 1;
hold on;
figure(4);
imshow(map);
while counterOfFirefliesNumber <= numOfFireflies
    newStart = start;
    rng(rngSeed);
    color = rand(3);
    while isGoalArrived==0
        [newStartPointVal, newStart, isGoalArrived, counterOfFirefliesNumber] = AddCandidatePointToPath(map, probabilisticMap, slope, newStart, round(goal), occupancyThresholdOfROI, counterOfFirefliesNumber);
        if isempty(fireflies{counterOfFirefliesNumber})
            fireflies{counterOfFirefliesNumber} = [start(1);start(2)];
            fireflies{counterOfFirefliesNumber} = cat(2, fireflies{counterOfFirefliesNumber},(newStart)');
           
            h3 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        else
            fireflies{counterOfFirefliesNumber} = cat(2, fireflies{counterOfFirefliesNumber},(newStart)');
            h4 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        end
    end
    if isGoalArrived > 0
        if isempty(fireflies{counterOfFirefliesNumber})
            fireflies{counterOfFirefliesNumber} = [start(1);start(2)];
            fireflies{counterOfFirefliesNumber} = cat(2, fireflies{counterOfFirefliesNumber},(newStart)');
            h5 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        else
            fireflies{counterOfFirefliesNumber} = cat(2, fireflies{counterOfFirefliesNumber},(newStart)');
            h6 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        end
        counterOfFirefliesNumber = counterOfFirefliesNumber +1;
        rngSeed = rngSeed +1;
        isGoalArrived =0;
    end
end
end