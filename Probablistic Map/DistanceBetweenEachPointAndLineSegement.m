function distance = DistanceBetweenEachPointAndLineSegement(sizeOfMap, Start, Goal)
rowsI = 1:sizeOfMap(1);
rows = double(rowsI);
colsI = 1:sizeOfMap(2);
cols = double(colsI);

% Indicies of each cell in the Map
MapInd = cat(3, repmat(rows',1,sizeOfMap(2)),repmat(cols,sizeOfMap(1),1));
StartPt = cat(3, repmat(Start(1),sizeOfMap(1),sizeOfMap(2)),repmat(Start(2),sizeOfMap(1),sizeOfMap(2)));
GoalPt = cat(3, repmat(Goal(1),sizeOfMap(1),sizeOfMap(2)),repmat(Goal(2),sizeOfMap(1),sizeOfMap(2)));

% Slope calculation
slope = (GoalPt(:,:,2) - StartPt(:,:,2)) ./ (GoalPt(:,:,1) - StartPt(:,:,1));

% intersection point between the perpendicular and the line
xIntersect = (slope ./ (1 + slope.^2)).*(MapInd(:,:,2) + MapInd(:,:,1) ./ slope + slope .* StartPt(:,:,1) - StartPt(:,:,2));
yIntersect = -xIntersect ./ slope + MapInd(:,:,2) + MapInd(:,:,1) ./ slope;
ytest = slope.*(xIntersect - StartPt(:,:,1))+StartPt(:,:,2);
distance = sqrt((MapInd(:,:,1) - xIntersect).^2 + (MapInd(:,:,2) - yIntersect).^2);
end