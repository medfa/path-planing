clc
close all;
clear all;
%% Initializaion
Goal = zeros(1,2);
Start = zeros(1,2);
alpha = 0.5;
coloredMap = imread('PathPlanningMap.png');
figure(1);
imshow(coloredMap);
grayMap = rgb2gray(coloredMap);
Map = imresize(grayMap, 1);
invMap = imcomplement(Map);
[numOfRows, numOfColumns] = size(invMap); %size of the map
figure(2);
imshow(grayMap);
hold on;
disp('Choose your start Point');
[yStartC, xStartC, but] = ginput(1);
h1 = text(yStartC,xStartC,'*', ...
                'HorizontalAlignment','center', ...
                'Color', [1 0 0], ...
                'FontSize',14);
xStart = int64(xStartC);
yStart = int64(yStartC);
Start = [xStartC, yStartC];

% Validation test of Start Point
if invMap(xStart,yStart) > 0
    disp('You chose not Valid Start Point, Try again!');
    return;
end
disp('Choose your Goal Point');
[yGoalC, xGoalC, but2] = ginput(1);      % get a point
h1 = text(yGoalC,xGoalC,'*', ...
                'HorizontalAlignment','center', ...
                'Color', [1 0 0], ...
                'FontSize',14);
xGoal = int64(xGoalC);
yGoal = int64(yGoalC);
Goal = [xGoalC, yGoalC];

% Validation test of Goal Point
if invMap(xGoal,yGoal) > 0
    disp('You chose unavailable Start Point, Try again!');
    return;
end

maxOccupation = max(invMap(:));
Probabilities = zeros(numOfRows,numOfColumns);
%% Find Probabilities
Distance = DistanceBetweenEachPointAndLineSegement([numOfRows, numOfColumns],Start, Goal);
maxDist = max(Distance(:));
Probabilities = alpha * (repmat(double(maxDist), numOfRows, numOfColumns) -...
    Distance) ./ repmat(double(maxDist), numOfRows, numOfColumns) + (1-alpha)* (repmat(double(maxOccupation), numOfRows, numOfColumns)-...
    double(invMap)) ./ repmat(double(maxOccupation), numOfRows, numOfColumns);

hold off;
figure(3);
imshow(Probabilities);

